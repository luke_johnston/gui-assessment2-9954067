﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment2_9954067
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            var subPriority = new List<string> {"Low Priority", "Medium Priority", "High Priority", "VERY HIGH PRIORITY!"};

            subjectCombo.ItemsSource = subPriority;

        }

        private async void buttonSend_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new MessageDialog(subjectText.Text);

            dialog.Title = "Your message:";
            dialog.Content = $"From: {fromInput.Text}" +Environment.NewLine + $"Subject: {subjectCombo.SelectedItem}" +Environment.NewLine + $"Message: {emailText.Text}";
            

            dialog.Commands.Add(new UICommand { Label = $"Send: {toInput.Text}", Id = 0 });

            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });

            dialog.Commands.Add(new UICommand { Label = "Reset", Id = 2 });

            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)

            { }

            if ((int)res.Id == 1)

            { }

            if ((int)res.Id == 2)

            {
                emailText.Text = "";
                subjectCombo.SelectedItem = "";
                toInput.Text = "";
                fromInput.Text = "";


            }
        }
    }
}
